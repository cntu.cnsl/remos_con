#!/usr/bin/env python

from __future__ import with_statement
from loguru import logger as L
import os
import sys
import errno
from fusepyng import FUSE, FuseOSError, Operations, fuse_get_context
from Command import Command
from config import Config


### Mount with security functions. ###
class SecuredMount(Operations):
    def __init__(self, root, container_id=None):
        self.root = root
        self.config = Config()
        # Open the resource sharing file
        try:
            with open(self.config.getMapping("image"), "r") as f:
                content = f.readlines()
            database_content = {}
            for line in content:
                line = line.replace("\n","")
                image_id = line.split(",")[1].strip()
                resources = line.split(",")[0].strip()
                if image_id in database_content:
                    database_content[image_id] += "," + resources
                else:
                    database_content[image_id] = resources
            self.db = database_content
        except Exception as e:
            L.error(f'Error: {e}')
            self.db = database_content
            
    # Helpers
    # =======
    def _full_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join(self.root, partial)
        return path

    # Filesystem methods
    # ==================
    def access(self, path, mode):
        full_path = self._full_path(path)
        if not os.access(full_path, mode):
            raise FuseOSError(errno.EACCES)

    def chmod(self, path, mode):
        full_path = self._full_path(path)
        return os.chmod(full_path, mode)

    def chown(self, path, uid, gid):
        full_path = self._full_path(path)
        return os.chown(full_path, uid, gid)

    def getattr(self, path, fh=None):
        full_path = self._full_path(path)
        st = os.lstat(full_path)
        return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                     'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))

    def formattedCommand(self, cmd):
        res = Command(f'{cmd}').run()[1].decode('utf-8')
        return res

    # Performance: 139 ms
    def getContainerShellGrep(self, access_pid):
        cmd = f'./utils/get_container_id.sh {access_pid}'
        c = Command(cmd)
        container_id = c.run()[1].decode('utf-8')[:12]
        return container_id

    # Performance: 85 ms
    def getContainerDirectCall(self, access_pid):
        # Performance 90 ms
        cmd = f'cat /proc/{access_pid}/mountinfo'
        c = Command(cmd)
        mounts = c.run()[1].decode('utf-8')
        # Fast collect of container_id
        container_id = ""
        for line in mounts.split("\n"):
            if "docker" not in line or "cgroup" not in line:
                continue
            container_id = line.split()[3].split("/")[2][:12]
        return container_id

    # Performance: 265 ms
    def getImageIdDirectCall(self, container_id):
        inspect_info = self.formattedCommand(f"docker inspect --format='{{.Image}}' {container_id}")
        image_id = inspect_info.split(":")[1][:12].strip()
        return image_id

    # Performance: 281 ms
    def getImageShellCall(self, container_id):
        cmd = f"./utils/get_image_id.sh {container_id}"
        inspect_info = Command(cmd).run()[1].decode('utf-8')
        image_id = inspect_info.split(":")[2][:12].strip()
        return image_id

    # Check if there is a pre-cached file
    # Performance: 178s
    def getImageFromCache(self, container_id):
        if not os.path.isfile(self.config.getMapping("image")):
            return None
        cmd = f'grep {container_id} {self.config.getMapping("image")}'
        out = Command(cmd).run()[1].strip()
        if not out:
            return None
        return out.decode('utf-8').split(",")[1][:12]

    def isPermitOpt2(self, file_name, access_pid):
        container_id = self.getContainerDirectCall(access_pid)
        if not container_id:
            return False
        with open(self.config.getMapping("container"), "r") as f:
            content = f.readlines()
        image_id = None
        for line in content:
            if container_id not in line:
                continue
            image_id = line.split(",")[1].strip()[:12]
            break
        if not image_id:
            return False
        if image_id not in self.db:
            return False
        if file_name not in self.db[image_id]:
            return False
        return True


    def isPermitOpt(self, file_name, access_pid):
        container_id = self.getContainerDirectCall(access_pid)
        if not container_id:
            return False
        image_id = self.getImageFromCache(container_id)
        if not image_id:
            image_id = self.getImageIdDirectCall(container_id)
        if image_id not in self.db:
            return False
        if file_name in self.db[image_id]:
            return True
        return False

    # # Access control
    def isPermit(self, file_name, access_pid):
        try:
            cmd = "docker ps -f 'status=running'"
            c = Command(cmd)
            list_con = c.run()[1].decode('utf-8')
            for line in list_con.split("\n"):
                if "CONTAINER ID" in line:
                    continue
                if not line.strip():
                    continue
                containerId = line.split()[0].strip()
                # L.info("ContainerId: {}. Line: {}".format(containerId, line))
                # Get process inside container
                out = self.formattedCommand(f"docker container top {containerId}")
                for line in out.split("\n"):
                    # Headline
                    if "UID" in line:
                        continue
                    if not line.strip():
                        continue
                    # Get pid inside container
                    print(line)
                    pid = line.split()[1].strip()
                    if int(pid) != access_pid:
                        continue
                    # Get the imageID
                    inspectInfo = self.formattedCommand(f"docker inspect {containerId}")
                    for line in inspectInfo.split("\n"):
                        # Make sure to get the first matched only
                        if "Image" not in line:
                            continue
                        if ":" not in line:
                            continue
                        ImageId = line.split(":")[2][:12].strip()
                        if ImageId not in self.db:
                            # L.info("DB: {}".format(self.db))
                            return False
                        if file_name in self.db[ImageId]:
                            # L.info("File is permitted. File: {}".format(file_name))
                            return True
                L.info("File is not permitted. File: {}".format(file_name))
                return False
        except Exception as e:
            # Unknown error
            L.error(f"Error:{e}")
            return False
        return False

    def readdir(self, path, fh):
        full_path = self._full_path(path)
        uid, gid, pid = fuse_get_context()
        dirents = ['.', '..']
        if os.path.isdir(full_path):
            dirents.extend(os.listdir(full_path))
        for r in dirents:
            # Access Control
            if not self.isPermitOpt(r, pid):
                continue
            yield r

    def readlink(self, path):
        pathname = os.readlink(self._full_path(path))
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    def mknod(self, path, mode, dev):
        return os.mknod(self._full_path(path), mode, dev)

    def rmdir(self, path):
        full_path = self._full_path(path)
        return os.rmdir(full_path)

    def mkdir(self, path, mode):
        return os.mkdir(self._full_path(path), mode)

    def statfs(self, path):
        full_path = self._full_path(path)
        stv = os.statvfs(full_path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))

    def unlink(self, path):
        return os.unlink(self._full_path(path))

    def symlink(self, name, target):
        return os.symlink(name, self._full_path(target))

    def rename(self, old, new):
        return os.rename(self._full_path(old), self._full_path(new))

    def link(self, target, name):
        return os.link(self._full_path(target), self._full_path(name))

    def utimens(self, path, times=None):
        return os.utime(self._full_path(path), times)

    # File methods
    # ============

    def open(self, path, flags):
        full_path = self._full_path(path)
        return os.open(full_path, flags)

    def create(self, path, mode, fi=None):
        full_path = self._full_path(path)
        return os.open(full_path, os.O_WRONLY | os.O_CREAT, mode)

    def read(self, path, length, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        return os.read(fh, length)

    def write(self, path, buf, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        return os.write(fh, buf)

    def truncate(self, path, length, fh=None):
        full_path = self._full_path(path)
        with open(full_path, 'r+') as f:
            f.truncate(length)

    def flush(self, path, fh):
        return os.fsync(fh)

    def release(self, path, fh):
        return os.close(fh)

    def fsync(self, path, fdatasync, fh):
        return self.flush(path, fh)

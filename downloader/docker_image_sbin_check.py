#!/usr/bin/env python
import os
from loguru import logger as L
from Command import Command

image_names = []
with open("list.txt","r") as _f:
    image_names = _f.readlines()

MAX = 30
count = 0
sbin_size = []
apps = []

def isAppAnalyzed(app):
    apps = []
    with open("blacklist.txt","r") as _f:
        apps = _f.readlines()
    with open("valid.txt","r") as _f:
        apps = apps.append(_f.readlines())
    for app_info in apps:
        if app in app_info:
            return True
    return False

def blacklist(app):
    flg = "a"
    if not os.path.isfile("blacklist.txt"):
        flg = "w"
    with open("blacklist.txt", flg) as _f:
        _f.write("{}\n".format(app))
    return

def valid(app, size):
    flg = "a"
    if not os.path.isfile("valid.txt"):
        flg = "w"
    with open("valid.txt", flg) as _f:
        _f2.write("{}, {}\n".format(app, size.split()[0].strip()))
    return

for app in image_names:
    valid_flg = "a"
    if count > MAX:
        break
    if isAppAnalyzed(app):
       count += 1
       continue
    # Download
    app = app.strip()
    L.info("Pulling docker image")
    os.system("docker pull {}".format(app))
    # Check the sbin
    try:
        L.info("Running and check for sbin resources")
        cmd = "docker run -d -it --rm {} bash".format(app)
        container_id = Command(cmd).run()[1].decode('utf-8').strip()
        if not container_id:
            blacklist(app)
            continue
        # Check the sbin
        cmd2  = "docker exec -it {} du -sh /sbin".format(container_id)
        size = Command(cmd2).run()[1].decode('utf-8').strip()
        if size == "0" or size == "OCI":
            blacklist(app)
            continue
        apps.append(app)
        sbin_size.append(size.split()[0].strip())
        valid(app, size)
        count += 1
        cmd3  = "docker stop {}".format(container_id)
        Command(cmd3).run()[1].decode('utf-8').strip()
    except:
        continue

res = zip(apps, sbin_size)
L.info("Results: {}".format(set(res)))

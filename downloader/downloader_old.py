#!/usr/bin/env python
import os
from loguru import logger as L

image_names = []
with open("list.txt","r") as _f:
    image_names = _f.readlines()

MAX = 20
count = 0
for app in image_names:
    if count > MAX:
        break
    # Strip names
    app = app.strip()
    L.info("Pulling docker image")
    os.system("docker pull {}".format(app))
    L.info("Done")
    count += 1
L.info("Done!!!")

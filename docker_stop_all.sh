#!/bin/bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
rm -f cache/container_maps
touch cache/container_maps

# RemoS_Con

## NAME: SecureShared Project

## How to start with the project ? 
->  Check and config the configuration files (config/config.ini)

## How to convert the original docker image ? 
### RUN

1) Execute the convert.sh ( or converter.py) with 2 arguments: imageID and sharedPath

For example: ./convert.sh 39a95ac32011 experiments/bash_sbin/

## How start the FUSE mount ? 

### CLI

Execute python cli.py:

1) The first input is either [run, exit]

2) The second input is shared resource path: experiments/lib (For example)

3) The third input is mountpoint: /tmp/test/ (For example)

### API (Recommended)

Execute python api.py

1) The first argument is shared resource path: experiments/lib (For example)

2) The second input is mountpoint: /tmp/test/ (For example)

# How to start the resourceless container ? 

1) Execute python deploy.py <imageID> <mountPoint>

- For example: python deploy.py bash_sbin_less /tmp/test/



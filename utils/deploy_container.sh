#!/bin/bash
image_id=$(docker image ls | grep $1 | awk -F' ' '{print $3}' | tail -n 1)
check_map=$(grep $1 $2 | wc -l)
# Write to cache
container_id=$(./utils/run_container.sh $3 $1)
if [ $check_map -lt 1 ]
then
    echo "$container_id,$image_id" >> $2
fi
echo "$container_id"

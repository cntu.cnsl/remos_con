#!/bin/bash
image_id=$(docker image ls | grep $@ | awk -F' ' '{print $3}' | tail -n 1)
check_map=$(grep $@ cache/image_maps | wc -l)
# Write to cache
container_id=$(./utils/run_container.sh $@)
if [ $check_map -lt 1 ]
then
    echo "$container_id,$image_id" >> cache/image_maps
fi

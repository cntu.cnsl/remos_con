#!/bin/sh
output=$(cat /proc/$@/mountinfo | grep docker | grep cgroup | head -n 1 | cut -d' ' -f4 | cut -d'/' -f3)
echo $output

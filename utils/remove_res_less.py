from Command import Command
import os

c = Command("docker image ls").run()[1].decode('utf-8')
for line in c.split("\n"):
    if "_less" in line:
        id = line.split()[2]
        os.system("docker rmi {} -f".format(id))


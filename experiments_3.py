#!/usr/bin/env python
from __future__ import unicode_literals, print_function
import sys
import os
import time
import threading

from loguru import logger as L
from Command import Command
from datetime import datetime


class Experiments():
    def __init__(self, target="bin"):
        self.target = target
        # Check if /tmp/test/ available
        if not os.path.isdir("/tmp/test"):
            os.makedirs("/tmp/test/")

    """
    Start using compose
    """
    def compose_start(self, path):
        os.system("docker-compose -f {} up -d".format(path))
        return

    """
    Build compose image (not run)
    """
    def compose_build(self, path):
        os.system("docker-compose -f {} build -d".format(path))
        return

    """
    Actually, we will stop all containers
    """
    def compose_stop(self):
        os.system("./docker_stop_all.sh")
        return

    def app_stop(self, app_id):
        os.system("docker stop {}".format(app_id))
        os.system("docker rm {}".format(app_id))
        return

    def app_cmd(self, app_id, cmd):
        cmd = "docker exec -it {} bash -c \"{}\"".format(app_id, cmd)
        ret = Command(cmd).run()
        return ret[1].decode("utf-8")

    def convert_app(self, app, target="sbin"):
        sharing_folder = "sharing/{}_{}_tmp".format(app, target)
        cmd = "python converter.py {} {} -S -R {}".format(app.strip(), sharing_folder, target)
        ret = Command(cmd).run()
        ret2 = ret[2].decode('utf-8').strip()
        L.info("cmd: {}. \n Convert information: {}\n".format(cmd, ret))
        return ret2

    def is_image_exist(self, app, tag=None):
        c = Command("docker image ls").run()[1].decode('utf-8')
        for line in c.split("\n"):
            if app not in line:
                continue
            if not tag:
                return True
            if tag not in line:
                continue
            return True
        return False

    def remove_resless(self, app):
        c = Command("docker image ls").run()[1].decode('utf-8')
        for line in c.split("\n"):
            if "_less" not in line:
                continue
            if app not in line:
                continue
            id = line.split()[2]
            os.system("docker rmi {} -f".format(id))

    ### If type == 0: Deployment, 1: Execution
    def time_converter(self, time, type=0, millisec=True):
        # time should be string
        mil = 1
        if millisec:
            mil = 1000
        if type == 0:
            hour = float(time.split(":")[0])
            minute = float(time.split(":")[1])
            second = float(time.split(":")[2])
            return hour*60 + minute*60 + second * mil
        # time should be as array []
        total_time = 0
        for t in time:
            minute = float(t.split("m")[0])
            second = float(t.split("m")[1][:-1]) # remove the s
            total_time += minute*60 + second
        return total_time*mil / len(time)

    def collectCommandTime(self,container_id):
        cmds_out = {}
        count = 0
        cmds = ["ls /lib", "ls /sbin", "ls /data", "echo 'test' > test.txt", "cat test.txt",
                "touch test2.txt", "rm -f test2.txt", "date", "mount", "ps", "mkdir test"]
        while(count < 5):
            # Collect command time
            for cmd in cmds:
                out = self.app_cmd(container_id, "time {}".format(cmd.strip()))
                for line in out.split("\n"):
                    if "real" not in line.strip():
                        continue
                    time = " ".join(line.split()[1:])
                    arg0 = cmd.split()[0]
                    key = arg0
                    try:
                        arg1 = cmd.split()[1]
                        if arg1 == "/lib" or arg1 == "/data" or arg1 == "/sbin":
                            key = "{}{}".format(arg0,arg1.replace("/","_"))
                    except:
                        pass
                    if key not in cmds_out:
                        cmds_out[key] = [time]
                    else:
                        cmds_out[key] += [time]
                    break
            count+=1
        return cmds_out
    
    def countFolders(self, container_id):
        cmds_out = ""
        cmds = ["ls /lib | wc -l", "ls /bin | wc -l", "ls /sbin | wc -l"]
        for cmd in cmds:
            out = self.app_cmd(container_id, "{}".format(cmd.strip()))
            if "lib" in cmd:
                cmds_out += "lib:{}".format(out) + "\n"
            elif "bin" in cmd:
                cmds_out += "bin:{}".format(out) + "\n"
            elif "sbin" in cmd:
                cmds_out += "sbin:{}".format(out) + "\n"
        return cmds_out


    def writeToFile(self,app, latex_output, count_output=None):
        if not os.path.isdir("experiments/ex3_report/"):
            os.makedirs("experiments/ex3_report/")
        flg = 'w'
        if os.path.isfile("experiments/ex3_report/{}.txt".format(app)):
            flg = 'a'
        with open("experiments/ex3_report/{}.txt".format(app), flg) as f:
            f.write("======== {} ========\n".format(app))
            f.write("EXECUTION: \n")
            f.write("Time: {}\n".format(latex_output))
            if count_output:
                f.write("Count {}".format(count_output))
            f.write("===================\n")
        return

    """
    In this experiments, we evaluate the access control in mixed-apps environment
    and on the multiple purpose containers
    """
    def ex3(self):
        # Check if there is a resless version of flask and redis
        composes = {"python":"3.7-alpine", "redis":None}
        resless = {}
        try:
            # Check whether the app images were installed 
            for app, tag in composes.items():
                if not self.is_image_exist(app, tag):
                    # Download
                    self.compose_build("experiments/ex3/docker-compose.yml")
                    continue
            # Exists ! Convert the compose app into resless version
            # Before that, prepare the total sharing folder
            base_folder = os.getcwd()
            target = os.path.join(base_folder, "sharing/ex3")
            L.info("Sharing folder: {}".format(target))
            if not os.path.isdir(target):
                os.makedirs(target)

            # Check if the resouceless version is exists
            resless = []
            for app, tag in composes.items():
                res_less = "{}_{}_less".format(app, self.target)
                if not self.is_image_exist(res_less, tag="latest"):
                    # Convert the image to image_less
                    try:
                        self.convert_app(app, self.target)
                    except Exception as e:
                        L.error("Error converting the app. Error: {}".format(e))
                        self.compose_stop()
                        sys.exit(1)
                # Merging the sharing content from 2 containers
                source = os.path.join(base_folder,"sharing/{}_{}_tmp".format(app, self.target))
                if not os.path.isdir(source):
                    L.error("There is no source folder. Please recheck the converter output")
                    sys.exit(1)
                os.system("cp -r {}/* {}/".format(source, target))
                resless.append(res_less)
            # Start the FUSE mount point
            os.system("python api.py {} {} &".format(target, "/tmp/test"))
            # Make sure the FUSE works
            count = 0
            fuse_flg = 0
            while(count < 10):
                check = Command("mount").run()[1].decode("utf-8").strip()
                for line in check.split("\n"):
                    if "/tmp/test" in line:
                        fuse_flg = 1
                        break
                count+=1
                time.sleep(0.5)
            if not fuse_flg:
                L.error("Error FUSE mount point is not available")
                sys.exit(1)
            # Start the compose and mount the FUSE mount point
            L.info("Starting resourceless containers")
            for res_app_name in resless:
                target = "bin"
                cmd = "python deploy.py {} {} -D ".format(res_app_name, target) # Non-stop API
                Command(cmd).run()
                #ret2 = ret[2].decode("utf-8").strip()
                #L.info("App: {} deployed".format(res_app_name))

            # Collect the information
            docker_list = Command("docker ps -a").run()[1].decode('utf-8')
            for line in docker_list.split('\n'):
                if not "bin_less" in line:
                    continue
                L.info("line: {}".format(line))
                container_id = line.split()[0].strip()
                app_name = line.split()[1].strip()
                L.info("Collecting commands from app: {}".format(app_name))
                exec_time_cmds = self.collectCommandTime(container_id)                
                count_cmds = self.countFolders(container_id)
                average_time_cmds = {}
                latex_output = ""
                for cmd, execs in exec_time_cmds.items():
                    times = self.time_converter(execs, 1)
                    average_time_cmds[cmd] = times
                    latex_output += "({},{}),".format(cmd, times)
                L.info("LATEX_OUTPUT: {}".format(latex_output))
                self.writeToFile(app_name, latex_output, count_cmds)

            # Stop the container
            try:
                cmd = "/bin/bash ./docker_stop_all.sh"
                Command(cmd).run()
            except:
                pass

            # Umount the FUSE
            cmd = "fusermount -u /tmp/test"
            Command(cmd).run()
            L.info("Done")
        except:
            # Stop the container
            try:
                cmd = "/bin/bash ./docker_stop_all.sh"
                Command(cmd).run()
            except:
                pass

            # Umount the FUSE
            cmd = "fusermount -u /tmp/test"
            Command(cmd).run()
            L.info("Done")

        return

ex_obj = Experiments("bin")
ex_obj.ex3()

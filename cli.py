#!/bin/bash
from __future__ import unicode_literals, print_function
from prompt_toolkit import PromptSession
from prompt_toolkit.completion import WordCompleter, PathCompleter,  merge_completers
import sys
import os
from loguru import logger as L

from fusepyng import FUSE, FuseOSError, Operations
from secured_mount import SecuredMount
from prompt_toolkit.styles import Style

style = Style.from_dict({
    'completion-menu.completion': 'bg:#008888 #ffffff',
    'completion-menu.completion.current': 'bg:#00aaaa #000000',
    'scrollbar.background': 'bg:#88aaaa',
    'scrollbar.button': 'bg:#222222',
    })
if __name__ == '__main__':
    cmd_completer = WordCompleter([
        'exit', 'run'
        ], ignore_case=True)

    _completer = merge_completers([PathCompleter(), cmd_completer])

    session = PromptSession(
        completer=_completer,
        complete_while_typing=True,
        style=style,
    )

    while True:
        try:
            text = session.prompt('Please run command> ')
        except KeyboardInterrupt:
            continue
        except EOFError:
            break
        else:
            if not text.strip():
                continue
            if text.strip() == "exit":
                L.info('GoodBye!')
                break
            if text.split()[0] == "run":
                # Check for paths
                # Get su priviledge
                root = session.prompt("Please give the source mount path> ")
                mountpoint = session.prompt("Please give the mounted path> ")
                if not os.path.isdir(root):
                    L.error("Incorrect path")
                    continue
                if not os.path.isdir(mountpoint):
                    L.error("Incorrect path")
                    continue
                FUSE(SecuredMount(root), mountpoint, nothreads=True, foreground=True, **{'allow_other': True})
                L.info("Goodbye!")

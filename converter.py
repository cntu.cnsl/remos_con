#!/usr/bin/env python
from __future__ import unicode_literals, print_function
import sys
import os
import optparse

from loguru import logger as L
from config import Config

from fusepyng import FUSE, FuseOSError, Operations
from secured_mount import SecuredMount
from utils import Utils
import shutil


util = Utils()
SOURCE_FOLDER = "/tmp/container_source"
silent = False

def isImageExist(image_id):
    images = util.formattedCommand("docker image ls")
    for line in images.split("\n"):
        # Header
        if "TAG" in line:
            continue
        if image_id not in line:
            continue
        # Get image respository
        image_name = line.split()[0].strip()
        return True, image_name
    return False, None

def runContainer(image_name):
    if not silent:
        L.info("Image name: {}".format(image_name))
    out = util.formattedCommand("docker run -d -it --rm {}".format(image_name))
    if not silent:
        L.info("Container is running")
    return out.strip()

def stopContainer(container_id):
    out = util.formattedCommand("docker stop {}".format(container_id))
    if not silent:
        L.info("Container stopped")
    return out

def copyFromGuestToTmp(container_id, folder):
    # Clear source folder
    if not folder:
        if not silent:
            L.error("Please specify the resource folder")
        return False
    if os.path.isdir(SOURCE_FOLDER):
        shutil.rmtree(SOURCE_FOLDER)
    os.makedirs(SOURCE_FOLDER)
    cmd = "docker cp {}:/{} {}".format(container_id, folder, SOURCE_FOLDER)
    if not silent:
        L.debug("Command: {}".format(cmd))
    out = util.formattedCommand(cmd)
    if not silent:
        L.info("Source are copied")
    return True

def copyTmpToTarget(image_id, target, folder):
    if not folder:
        if not silent:
            L.error("Please specify the target folder")
    source = os.path.join(SOURCE_FOLDER, folder)
    info_to_file = []
    if not os.path.isdir(target):
        os.makedirs(target)
    for src_dir, dirs, files in os.walk(source, topdown=True):
        dst_dir = src_dir.replace(source, target, 1)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        for file_ in files:
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                # in case of the src and dst are the same file
                try:
                    if os.path.samefile(src_file, dst_file):
                        continue
                    os.remove(dst_file)
                except:
                    pass
            try:
                shutil.move(src_file, dst_dir)
                # write the moved file to library information
                info_to_file.append(file_)
            except:
                # File might not exists (symbolic)
                continue
        for dir_ in dirs:
            src_dir = os.path.join(src_dir, dir_)
            dst_dir = os.path.join(dst_dir, dir_)
            if os.path.exists(dst_dir):
                # in case of the src and dst are the same file
                if os.path.samefile(src_dir, dst_dir):
                    continue
            # write the moved file to library information
            info_to_file.append(dir_)
    return info_to_file


""" Remove the target folder. """
def writeResInfo(image_id, info_to_file):
    write_flg = 'a'
    if not os.path.isfile(config.getMapping("image")):
        write_flg = 'w'
    with open(config.getMapping("image"), write_flg) as _f:
        for info in info_to_file:
            if not info:
                continue
            _f.write("{}, {}\n".format(info, image_id))
            if not silent:
                L.info("File: {} is wroted to the storage folder".format(info))
    return

""" Remove the target folder.  """
def removeTargetFolder(container_id, image_name, folder):
    if not silent:
        L.info("Removing target folder...")
    new_name = "{}_{}_less".format(image_name, folder)
    util.formattedCommand("docker exec -t {} rm -rf {}".format(container_id, folder))
    # Create empty folder
    util.formattedCommand("docker exec -t {} mkdir {}".format(container_id, folder))
    tag = util.formattedCommand("docker commit {}".format(container_id))
    util.formattedCommand("docker tag {} {}".format(tag, new_name))
    if not silent:
        L.info("Done")
    return tag

if __name__=="__main__":
    parser = optparse.OptionParser("usage: %prog [options] imageID sharedPath")
    parser.add_option("-S", "--silent", dest="silent", default="",
            action="store_true", help="Silent the Log")
    parser.add_option("-R", "--resource", dest="resource", default="",
            help="Target resource")

    (options, args) = parser.parse_args()
    if len(args) != 2:
        parser.error("incorrect number of arguments")
    config = Config()
    image_id = args[0]
    out_path = args[1]
    silent = options.silent
    target = options.resource

    result, image_name = isImageExist(image_id)
    if not result:
        if not silent:
            L.error("ImageID is not exists")
        sys.exit(1)
    if not os.path.isdir(out_path):
        os.makedirs(out_path)
    container_id = runContainer(image_name)
    if not target:
        target = config.getTargetFolder()
    ret = copyFromGuestToTmp(container_id, target)
    if not ret:
        sys.exit(1)
    info_to_file = copyTmpToTarget(image_id, out_path, target)
    if not info_to_file:
        sys.exit(1)
    tag = removeTargetFolder(container_id, image_name, target)
    # only get 12 first number of the image_id
    tag = tag.split(":")[1][:12]
    writeResInfo(tag, info_to_file)
    stopContainer(container_id)

#! /usr/bin/env python
from __future__ import unicode_literals, print_function
import sys
import os
import optparse
from loguru import logger as L
from Command import Command
from config import Config

if __name__=="__main__":
    parser = optparse.OptionParser("usage: %prog imageID mount_folder")
    parser.add_option("-S", "--stop", dest="stop", default="",
            action="store_true", help="Stop the app after deploy")
    parser.add_option("-D", "--daemon", dest="daemon", default="",
            action="store_true", help="Deploy as daemon")

    (options, args) = parser.parse_args()
    if len(args) != 2:
        parser.error("incorrect number of arguments")
    config = Config()
    image_id = args[0]
    mount = args[1]
    stop = options.stop
    daemon = options.daemon
    cmd = "./utils/deploy_container.sh {} {} {}".format(image_id, config.getMapping("container"), mount)
    L.info("Deploy: {}".format(cmd))
    c = Command(cmd)
    container_id = c.run()[1].decode('utf-8')[:12]
    if not container_id.strip():
        L.error("Error deploying container")
        sys.exit(1)
    # Read image-lib mapping
    with open(config.getMapping("image"),"r") as _f:
        content = _f.readlines()
    # container-lib mapping
    lib_seq = ""
    for line in content:
        if image_id in line:
            lib = line.split(",")[0].strip()
            if not lib_seq:
                lib_seq = lib
            else:
                lib_seq += "," + lib
    # Write to cache folder
    flg = "a"
    if not os.path.isfile(config.getMapping("container")):
        flg = "w"
    with open(config.getMapping("container"), flg) as _f2:
        _f2.write("{},{}".format(container_id, lib_seq))
    L.info("CONTAINER_ID: {}".format(container_id))
    if daemon:
        os.system("docker exec -d -it {} bash".format(container_id))
    else:
        os.system("docker exec -it {} bash".format(container_id))
    if stop:
        os.system("docker stop {}".format(container_id))

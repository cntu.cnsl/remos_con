#!/usr/bin/env python
from __future__ import unicode_literals, print_function
import sys
import os
import time
import threading

from loguru import logger as L
from Command import Command
from datetime import datetime


class Experiments():
    def __init__(self, number=1):
        self.ex_size = []
        self.ex_app = []
        if number == 1:
            with open("experiments/ex1/ex_app.txt","r") as _f:
                self.ex_app = _f.readlines()
            with open("experiments/ext1/ex_size.txt", "r") as _f2:
                self.ex_size = _f2.readlines()

    def app_start(self, app):
        cmd = "./utils/docker_run_daemon.sh ".format(app)
        app_id = Command(cmd).run()[1].decode("utf-8")
        return app_id

    def app_stop(self, app_id):
        os.system("docker stop {}".format(app_id))
        os.system("docker rm {}".format(app_id))
        return

    def app_cmd(self, app_id, cmd):
        cmd = "docker exec -it {} bash -c \"{}\"".format(app_id, cmd)
        ret = Command(cmd).run()
        return ret[1].decode("utf-8")

    def convert_app(self, app, target="sbin"):
        sharing_folder = "sharing/{}_{}_tmp".format(app, target)
        cmd = "python converter.py {} {} -S -R {}".format(app.strip(), sharing_folder, target)
        ret = Command(cmd).run()
        ret2 = ret[2].decode('utf-8').strip()
        L.info("cmd: {}. \n Convert information: {}\n".format(cmd, ret))
        return ret2

    def is_image_exist(self, app):
        c = Command("docker image ls").run()[1].decode('utf-8')
        for line in c.split("\n"):
            if app not in line:
                continue
            return True
        return False

    def remove_resless(self, app):
        c = Command("docker image ls").run()[1].decode('utf-8')
        for line in c.split("\n"):
            if "_less" not in line:
                continue
            if app not in line:
                continue
            id = line.split()[2]
            os.system("docker rmi {} -f".format(id))

    ### If type == 0: Deployment, 1: Execution
    def time_converter(self, time, type=0, millisec=True):
        # time should be string
        mil = 1
        if millisec:
            mil = 1000
        if type == 0:
            hour = float(time.split(":")[0])
            minute = float(time.split(":")[1])
            second = float(time.split(":")[2])
            return hour*60 + minute*60 + second * mil
        # time should be as array []
        total_time = 0
        for t in time:
            minute = float(t.split("m")[0])
            second = float(t.split("m")[1][:-1]) # remove the s
            total_time += minute*60 + second
        return total_time*mil / len(time)

    def ex1_save(self, app, time):
        flg = "a"
        if not os.path.isfile("experiments/ex1/output.txt"):
            flg = "w"
        with open("experiments/ex1/output.txt", flg) as f:
            f.write("{},{}\n".format(app, time))

    def ex1(self):
        target = "sbin"
        for app in self.ex_app:
            try:
                # Example:
                # date  0.00s user 0.00s system 85% cpu 0.003 total
                cmd = "time python converter.py {} sharing/temp/ -S -R {}".format(app.strip(), target)
                c = Command(cmd).run()[2].decode('utf-8').strip()
                time = c.split()[0][:4].strip()
                self.ex1_save(app.strip(), time)
            except Exception as e:
                L.error("Error:{}".format(e))
                continue
        return

    """
    Get execution time for the commands
    """
    def ex2_getTime(self, app, cmds=None):
        app_id = self.app_start(app).strip()
        try:
            L.info("Deployed {} app with AppID: {}".format(app, app_id))
            if not cmds:
                self.app_stop(app_id)
                # We only care about the start/stop time
                return 0
            # Determine the time to execute commands
            # cmds should be array of command
            self.ex2_data_preparation(app, app_id)
            cmds_out = {}
            count = 0
            while(count < 5):
                ## Calculate 3 times
                for cmd in cmds:
                    out = self.app_cmd(app_id, "time {}".format(cmd.strip()))
                    for line in out.split("\n"):
                        if "real" not in line.strip():
                            continue
                        time = " ".join(line.split()[1:])
                        arg0 = cmd.split()[0]
                        key = arg0
                        try:
                            arg1 = cmd.split()[1]
                            if arg1 == "/lib" or arg1 == "/data" or arg1 == "/sbin":
                                key = "{}{}".format(arg0,arg1.replace("/","_"))
                        except:
                            pass
                        if key not in cmds_out:
                            cmds_out[key] = [time]
                        else:
                            cmds_out[key] += [time]
                        break
                count+=1
            self.app_stop(app_id)
            return cmds_out
        except Exception as e:
            L.error("Error: {}".format(e))
            self.app_stop(app_id)
            return None
        self.app_stop(app_id)
        return None

    ### This one for checking the deployment time of original app
    def ex2_check_dep_ori(self, app):
        start=datetime.now()
        # Just deployment Time
        ret = self.ex2_getTime(app)
        dep_time = str(datetime.now()-start)
        # Convert
        dep_time = self.time_converter(dep_time, 0)
        return dep_time

    def ex2_check_cmds_ori(self, app):
        # Check time of commands
        cmds = ["ls /lib", "ls /sbin", "ls /data", "echo 'test' > test.txt", "cat test.txt", "touch test2.txt", "rm -f test2.txt", "date", "mount", "ps", "mkdir test"]
        exec_time_cmds = self.ex2_getTime(app, cmds)
        # Convert
        average_time_cmds = {}
        latex_output = ""
        for cmd, execs in exec_time_cmds.items():
            times = self.time_converter(execs, 1)
            average_time_cmds[cmd] = times
            latex_output += "({},{}),".format(cmd, times)
        return latex_output

    """ ex2_check_cmds_resless
    Evaluate the time for executing each command in the container
    """
    def ex2_check_cmds_resless(self, app_id):
        cmds = ["ls /lib", "ls /sbin", "ls /data", "echo 'test' > test.txt", "cat test.txt", "touch test2.txt", "rm -f test2.txt", "date", "mount", "ps", "mkdir test_folder"]
        cmds_out = {}
        count = 0
        while(count < 5):
            ## Calculate 5 times
            for cmd in cmds:
                out = self.app_cmd(app_id, "time {}".format(cmd.strip()))
                for line in out.split("\n"):
                    if "real" not in line.strip():
                        continue
                    time = " ".join(line.split()[1:])
                    arg0 = cmd.split()[0]
                    key = arg0
                    try:
                        arg1 = cmd.split()[1]
                        if arg1 == "/lib" or arg1 == "/data" or arg1 == "/sbin":
                            key = "{}{}".format(arg0,arg1.replace("/","_"))
                    except:
                        pass

                    if key not in cmds_out:
                        cmds_out[key] = [time]
                    else:
                        cmds_out[key] += [time]
                    break
            count+=1
        return cmds_out

    """ ex2_metric_res
    Evaluate the resless container with different metrics
    """
    def ex2_metric_res(self, app, target="sbin"):
        # Make sure there is no _less image
        self.remove_resless(app)
        self.convert_app(app, target)
        res_app_name = "{}_{}_less".format(app, target)
        sharing_folder = "sharing/{}_{}_tmp".format(app, target)
        dep_time = None
        dep_time_ori = None
        # FUSE_sharing
        if not os.path.isdir("/tmp/test/"):
            os.makedirs("/tmp/test/")
        ### This one for checking the deployment time of resless app
        ### Except for the lib, since the /usr/bin is not work without /lib
        if target != "lib":
            count = 0
            dep_time = 0
            dep_time_ori = 0
            while (count < 5):
                tmp_time = self.ex2_check_dep_ori(res_app_name)
                tmp_time_ori = self.ex2_check_dep_ori(app)
                dep_time += tmp_time
                dep_time_ori += tmp_time_ori
                count+=1
            dep_time = dep_time / 5
            dep_time_ori = dep_time_ori / 5
            L.info("DEP RESLESS: {}".format(dep_time))
            L.info("DEP ORIGIN: {}".format(dep_time_ori))

            # cmd = "python deploy.py {} {} -D -S".format(res_app_name, target) # Stop API
        try:
            ### This one for checking the execution
            # Prepare the FUSE mountpoint
            os.system("python api.py {} {} &".format(sharing_folder, "/tmp/test"))
            ### FOR DEBUG
            # Make sure the FUSE works
            count = 0
            fuse_flg = 0
            while(count < 10):
                check = Command("mount").run()[1].decode("utf-8").strip()
                for line in check.split("\n"):
                    if "/tmp/test" in line:
                        fuse_flg = 1
                        break
                count+=1
                time.sleep(0.5)
            if not fuse_flg:
                L.error("Error FUSE mount point is not available")
                sys.exit(1)
            #  Deploy the resless container as daemon
            start=datetime.now()
            cmd = "python deploy.py {} {} -D ".format(res_app_name, target) # Non-stop API
            L.info("execute deploy: {}".format(cmd))
            ret = Command(cmd).run()
            L.info("Return: {}".format(ret))
            ret2 = ret[2].decode("utf-8").strip()
            if target == "lib":
                dep_time = str(datetime.now()-start)
                dep_time = self.time_converter(dep_time, 0)
            container_id = None
            for line in ret2.split("\n"):
                if "CONTAINER_ID" in line:
                    container_id = line.split(":")[-1].strip()
            L.info("Container ID {} deployed".format(container_id))
            # container_list = Command("docker ps -a").run()[1].decode("utf-8")
            # Copy the data into container
            # self.ex2_data_preparation(app, container_id)
            # Executing
            exec_time_cmds = self.ex2_check_cmds_resless(container_id)
            # Convert
            average_time_cmds = {}
            latex_output = ""
            for cmd, execs in exec_time_cmds.items():
                times = self.time_converter(execs, 1)
                average_time_cmds[cmd] = times
                latex_output += "({},{}),".format(cmd, times)

            # Stop the container
            if container_id:
                try:
                    self.app_stop(container_id)
                    cmd = "/bin/bash ./docker_stop_all.sh"
                    Command(cmd).run()
                except:
                    pass

            # Umount the FUSE
            cmd = "fusermount -u /tmp/test"
            Command(cmd).run()
            L.info("Done")
        except Exception as e:
            # Finalize
            # Stop all container
            L.error("Error: {}".format(e))
            cmd = "/bin/bash ./docker_stop_all.sh"
            Command(cmd).run()
            # Umount the FUSE
            cmd = "fusermount -u /tmp/test"
            Command(cmd).run()
            L.info("Done")
        return dep_time_ori,  dep_time, latex_output

    """ ex2_metric
    Get deployment time and execution time for original container
    """
    def ex2_metric(self, app):
        dep_time = self.ex2_check_dep_ori(app)
        exec_time = self.ex2_check_cmds_ori(app)
        return dep_time, exec_time

    """ ex2_data_preparation
    Prepare the data folder for a target docker container
    """
    def ex2_data_preparation(self, app, app_id):
        cmd = "docker exec -it {} mkdir /data".format(app_id)
        Command(cmd).run()
        cmd = "cp -r experiments/data/ sharing/{}_data_tmp".format(app)
        c = Command(cmd).run()
        return

    def ex2_createNewData_image(self, app):
        current_folder = os.getcwd()
        data_folder = os.path.join(current_folder, "experiments/data")
        L.info('data folder: {}'.format(data_folder))
        new_name = "{}_data".format(app)
        app_id = self.app_start(app)
        Command("docker cp {} {}:/".format(data_folder, app_id[:12])).run()
        out = Command("docker exec -it {} ls /".format(app_id)).run()[1].decode('utf-8')
        tag = Command("docker commit {}".format(app_id)).run()[1].decode("utf-8")
        Command("docker tag {} {}".format(tag, new_name)).run()
        return

    def ex2(self):
        apps = ['wordpress', 'ubuntu-debootstrap']
        flg = 'w'
        # Prepare each version with new
        # prepare new docker image
        for app in apps:
            data_app = "{}_data".format(app)
            if not self.is_image_exist(data_app):
                self.ex2_createNewData_image(app)
            # dep_ori_sbin, dep_sbin, exec_sbin = self.ex2_metric_res(data_app, "sbin")
            # dep_ori_data, dep_data, exec_data = self.ex2_metric_res(data_app, "data")
            # There is no deployment evaluation for Lib (since there is no library for apps)
            dep_ori_lib, dep_lib, exec_lib = self.ex2_metric_res(data_app, "lib")
            dep_ori, exec_ori = self.ex2_metric(data_app)
            if not os.path.isdir("experiments/ex2"):
                os.makedirs("experiments/ex2")
            if os.path.isfile("experiments/ex2/{}.txt".format(app)):
                flg = 'a'
            with open("experiments/ex2/{}.txt".format(app), flg) as f:
                f.write("======== {} ========\n".format(app))
                f.write("DEPLOYMENT: \n")
                f.write("Original Container: {}\n".format(dep_ori))
                # f.write("Container sbin_no_fuse: {}. sbin_fuse: {}\n".format(dep_ori_sbin, dep_sbin))
                # f.write("Container data_no_fuse: {}. data_fuse: {}\n".format(dep_ori_data, dep_data))
                f.write("Container lib_no_fuse: NA. lib_fuse: {}\n".format(dep_lib))
                f.write("===================\n")
                f.write("EXECUTION: \n")
                f.write("original_execs: {}\n".format(exec_ori))
                # f.write("sbin_execs: {}\n".format(exec_sbin))
                # f.write("data_execs: {}\n".format(exec_data))
                f.write("lib_execs: {}\n".format(exec_lib))
                f.write("===================\n")

ex_obj = Experiments(2)
ex_obj.ex2()

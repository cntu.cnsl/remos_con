#!/usr/bin/env python
from __future__ import with_statement
import configparser

class Config():
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config/config.ini')
        self.config = config

    def getMapping(self, type="container"):
        if type == "image":
            return self.config['Mapping']['image_maps']
        return self.config['Mapping']['container_maps']

    def getTargetFolder(self):
        return self.config['Resources']['target_folder']


#!/usr/bin/env python3
from __future__ import unicode_literals, print_function
import sys
import os
from Command import Command

class Utils():
    def formattedCommand(self, cmd):
        res = Command("{}".format(cmd)).run()[1].decode('utf-8')
        return res

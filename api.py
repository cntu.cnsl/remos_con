#!/bin/bash
from __future__ import unicode_literals, print_function
import sys
import os
import optparse

from loguru import logger as L

from fusepyng import FUSE, FuseOSError, Operations
from secured_mount import SecuredMount


if __name__=="__main__":
    parser = optparse.OptionParser("usage: %prog [options] source_mount target_mount")
    (options, args) = parser.parse_args()
    if len(args) < 2:
        parser.error("incorrect number of arguments")
    source = args[0]
    target = args[1]

    if not os.path.isdir(source):
        L.error("Incorrect path")
        sys.exit(1)
    if not os.path.isdir(target):
        L.error("Incorrect path")
        sys.exit(1)
    L.info("Start the fuse function")
    FUSE(SecuredMount(source), target, nothreads=False, foreground=True,max_read=131072, **{'allow_other': True})
    L.info("Goodbye!")

